---
title: 'Hugo(构建网站最快的框架)的安装与使用'
date: 2025-01-14T20:27:34+08:00
description: "本文介绍Hugo的手动安装与使用，安装环境为macOS(arm64/Apple Silicon)。"
isCJKLanguage: true
draft: false

summary: "本文介绍Hugo的手动安装与使用，安装环境为macOS(arm64/Apple Silicon)。"

keywords:
- "hugo"
- "Install"
- "blog"
- "博客"
- "hugo template"
- "hugo模板"
- "Dart Sass"
- "git 子模块"
- "git submodule"

categories:
- "dev"

tags:
- "hugo"
- "go"
- "dart"
- "sass"
- "git"
- "macOS"


---

> 构建网站最快的框架。(The world’s fastest framework for building websites) -- [Hugo](https://gohugo.io/)

## 1. Hugo简介

Hugo使用Go语言模板语法进行静态网站内容的生成，结合主题模板是创建博客(blog)的最佳利器。自v0.128.0起，安装程序除标准版外，还提供扩展版(extended edition)及扩展/部署版(extended/deploy edition)。所谓扩展版即在标准版基础上集成了对WebP格式处理的功能、内嵌[Transpile Sass to CSS](https://gohugo.io/hugo-pipes/transpile-sass-to-css/)库、[Dart Sass](https://gohugo.io/hugo-pipes/transpile-sass-to-css/#dart-sass)功能，扩展/部署版在扩展版基础上提供直接部署到Google云存储(Google Cloud Storage)、AWS S3和Azure云存储(Azure Storage)的功能。

## 2. 所需软件

安装Hugo前，请提前安装所需软件包括：
-   [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
-   [Go](https://go.dev/doc/install)
-   [Dart Sass](https://gohugo.io/hugo-pipes/transpile-sass-to-css/#dart-sass)

## 3. 下载Hugo

根据自己计算机的芯片类型和操作系统，从Hugo官方[最新发行](https://github.com/gohugoio/hugo/releases/latest) 选择相应的发行版的安装包进行下载。以Hugo v0.142.2扩展版在macOS(arm64/Apple Silicon)为例，可在终端(Terminal)中执行以下命令行进行下载，并使用sha256sum命令进行下载文件完整性校验：

```bash
mkdir -p /opt/hugo
cd /opt/hugo
wget -c "https://github.com/gohugoio/hugo/releases/download/v0.140.2/hugo_extended_0.140.2_darwin-universal.tar.gz"
echo "452646d6fde82d1890db1805c29be983337c32cd7858a135422636ea6547daf8  hugo_extended_0.140.2_darwin-universal.tar.gz" | sha256sum -c -
```

## 4. Hugo的安装与环境配置

以Hugo v0.140.2扩展版安装/opt/hugo/0.140.2/下为例进行说明，可在终端(Terminal)中执行以下命令：

```bash
mkdir -p /opt/hugo/0.140.2
tar -C /opt/hugo/0.140.2 -zxvf hugo_extended_0.140.2_darwin-universal.tar.gz
rm -fr hugo_extended_0.140.2_darwin-universal.tar.gz
```

配置Hugo相关的环境变量，可将以下SHELL代码片段添加到.bashrc或.profile中，推荐保存到/etc/profile.d/hugo.sh中:

```bash
#!/bin/sh

if [ -z "${HUGO_HOME}" ] ; then
    # hugo_version="0.127.0"
    hugo_version="0.140.2"
    hugo_path="/opt/hugo/${hugo_version}"

    if [ -x "${hugo_path}/hugo" ] ; then
        HUGO_HOME="${hugo_path}" ; export HUGO_HOME
        
        PATH=${HUGO_HOME}:${PATH} ; export PATH
    fi

    unset hugo_path
    unset hugo_version
fi
```

hugo自带shell命令补全生成命令，使用shell命令补全可在终端(Terminal)中执行以下命令：

```bash
hugo completion bash > /opt/homebrew/etc/bash_completion.d/hugo
```

其中，hugo completion支持bash、zsh、fish、powershell等shell命令补全。

## 5. Hugo创建(博客)网站的一般步骤

### 5.1 创建网站目录结构及配置文件

使用Hugo命令，以下以创建demo网站并使用[Ananke](https://github.com/theNewDynamic/gohugo-theme-ananke)主题模板为例，说明Hugo创建网站包括博客的一般步骤为：

```bash
hugo new site demo
cd demo
git init
git submodule add https://github.com/theNewDynamic/gohugo-theme-ananke.git themes/ananke
echo "theme = 'ananke'" >> hugo.toml
hugo server -D
```

其中，

-   `hugo new site demo`： 用来生成网站的目录结构；
-   `git init`：git版本控制初始化；
-   `git submodule add https://github.com/theNewDynamic/gohugo-theme-ananke.git themes/ananke`：作为git子模块，克隆Ananke主题到themes/ananke目录下。
-   `echo "theme = 'ananke'" >> hugo.toml`：在生成网站的配置文件中，配置ananke主题。
-   `hugo server -D`：启动Hugo服务器的开发模式以预览站点，可使用`Ctrl + C`来停止Hugo服务器。

#### 5.1.1 Hugo的网站目录结构

执行`hugo new site my-site`命令，生成的目录结构如下：

```bash
my-site/
├── archetypes/
│   └── default.md
├── assets/
├── content/
├── data/
├── i18n/
├── layouts/
├── static/
├── themes/
└── hugo.toml         <-- 网站配置(site configuration)
```

如果使用子目录的方式，网站目录结构可为：

```bash
my-site/
├── archetypes/
│   └── default.md
├── assets/
├── config/           <-- 网站配置(site configuration)
│   └── _default/
│       └── hugo.toml
├── content/
├── data/
├── i18n/
├── layouts/
├── static/
└── themes/
```

### 5.2 新建页面

-   `hugo new content content/posts/my-first-post.md`：在content/posts目录下，根据archetypes/default.md模板新建页面；

页面使用Markdown文法，包括前言(Front matter)和正文部分。其中前言(Front matter)在页面顶部是页面的元数据(metadata)，通常包括：

-   对页面内容的描述；
-   页面内容的参数；
-   与其他页面的关系；
-   控制站点的生成结构；
-   决定模板的选择。

支持的格式包括：[JSON](https://www.json.org/)、[TOML](https://toml.io/)和[YAML](https://yaml.org/)，Hugo通过前言区的分隔符来决定前言区的格式。

YAML格式的前言区如：
```yaml
---
date: 2024-02-02T04:14:54-08:00
draft: false
params:
  author: John Smith
title: Example
weight: 10
---
```

TOML格式的前言区如：
```toml
+++
date = 2024-02-02T04:14:54-08:00
draft = false
title = 'Example'
weight = 10
[params]
  author = 'John Smith'
+++
```

JSON格式的前言区如：
```json
{
   "date": "2024-02-02T04:14:54-08:00",
   "draft": false,
   "params": {
      "author": "John Smith"
   },
   "title": "Example",
   "weight": 10
}
```
前言区的字段类型可以为boolean、integer、float、string、arrays或者maps等Go语言支持的类型。常用的字段有：

-   `date` (string)：页面关联的时间，通常是页面的创建时间。在模版中，可使用`Page`对象的`Date`方法来获取其值；
-   `draft` (boolean)：如果为`true`，在使用`hugo`生成正式的部署文件时，该页面不被生成，除非使用`--buildDrafts`参数。在模版中，可使用`Page`对象的`Draft`方法来获取其值；
-   `title` (string)：页面的标题，在模版中，可使用`Page`对象的`Title`方法来获取其值；
-   `weight` (integer)：在页面集(page collection)中进行排序。在模版中，可使用`Page`对象的`Weight`方法来获取其值。

如果Hugo的网站配置文件中，配置了分类术语，则可在前言区中对页面的分类进行设定。如`hugo.yaml`：
```yaml
taxonomies:
  genre: genres
  tag: tags
```

`content/example.md`：

```yaml
---
date: 2024-02-02T04:14:54-08:00
draft: false
genres:
- mystery
- romance
params:
  author: John Smith
tags:
- red
- blue
title: Example
weight: 10
---
```

可以为支持的页面类型在其前言区，设置分类术语。在模版中，可使用`Page`的`Params`或`GetTerms`来获取分类术语，如：

```go
{{ with .GetTerms "tags" }}
  <p>Tags</p>
  <ul>
    {{ range . }}
      <li><a href="{{ .RelPermalink }}">{{ .LinkTitle }}</a></li>
    {{ end }}
  </ul>
{{ end }}
```

支持的页面类型包括：`home`, `page`, `section`, `taxonomy`, or `term`，对应的目录结构位置为：

```bash
content/
├── books/
│   ├── book-1/
│   │   └── index.md    <-- kind = page
│   ├── book-2.md       <-- kind = page
│   └── _index.md       <-- kind = section
├── tags/
│   ├── fiction/
│   │   └── _index.md   <-- kind = term
│   └── _index.md       <-- kind = taxonomy
└── _index.md           <-- kind = home
```

更详细的前言区字段说明，请参考[Front matter文档](https://gohugo.io/content-management/front-matter/)。

### 5.3 生成网站与部署

-   `hugo`或`hugo --gc --minify`：生成可部署的网站内容，通常会生成public、resources目录。

```bash
my-site/
├── archetypes/
│   └── default.md
├── assets/
├── config/       
│   └── _default/
│       └── hugo.toml
├── content/
├── data/
├── i18n/
├── layouts/
├── public/       <-- build时创建(created when you build your site)
├── resources/    <-- build时(created when you build your site)
├── static/
└── themes/
```

最终，生成的网站内容位于`public`目录下，将其下的内容部署到网站服务器上即可。
